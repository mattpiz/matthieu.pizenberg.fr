+++
date = "2017-06-18"
draft = false
tags = ["git"]
title = "Introduction to Git"
math = false
+++

This is a [brief introduction to Git][git_intro]
I prepared during our _projet long_ at ENSEEIHT (back in 2015).
In this document, I recap the main features of Git to explain why it is so awesome!
I follow the same plan as the one of [that book][git_v2]
but I keep only vital information.<!--more-->

[git_intro]: https://drive.google.com/file/d/0B5LGw6hAyk6DNkFQa2lZSHUwc0U/view?usp=sharing
[git_v2]: http://git-scm.com/book/en/v2
