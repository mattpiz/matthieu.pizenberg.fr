+++
# Date this page was created.
date = "2017-06-18"

# Project title.
title = "MATRIX"

# Project summary to display on homepage.
summary = "MVG (Multiple View Geometry) Assistant Tool for Reconstruction from Images eXtraction."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "posts/matrix.png"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["computer-vision"]

# Optional external URL for project (replaces project detail page).
# external_link = "https://github.com/OeufsDePie/MATRIX"

# Does the project detail page use math formatting?
math = false

# Optional featured image (relative to `static/img/` folder).
[header]
image = "posts/matrix_header.png"
caption = ""

+++

MATRIX ([github][matrix]):
MVG (Multiple View Geometry) Assistant Tool for Reconstruction from Images eXtraction.

[matrix]: https://github.com/OeufsDePie/MATRIX

![matrix](/img/posts/matrix.png)

## What is MATRIX ?

MATRIX is a project we developed in the context of a last-year 7-week school group project in 2015.
This project aimed at developing a tool to ease the task of collecting a dataset of pictures of a
scene for 3D reconstruction. The project was casted in the context of the European Project
[POPART](http://www.popartproject.eu/),
in which the team [VORTEX](http://www.irit.fr/-Equipe-VORTEX-)
and [Mikros Image](http://www.mikrosimage.eu/) are involved.
POPART aims at developing tools for the realime previsualization of special effects
during the shooting of a movie.
In such context, the preliminary task is to collect a dataset of images of the scene
where the movie will take place and perform an accurate 3D reconstruction of the scene,
that will later be used for realtime camera localization purpose.

{{< youtube frpuu8nP56w >}}

## Documentation

You will find the documentation of the project on that page:
http://oeufsdepie.github.io/MATRIX/index.html

## Related projects and useful resources

* [OpenMVG](https://github.com/openMVG/openMVG/):
a library for computer-vision scientists and especially targeted to the Multiple View Geometry community.
* [libgphoto](http://www.gphoto.org/):
a library designed to allow access to digital camera by external programs.
* [Qt](http://www.qt.io/):
a cross-platform application framework for developing softwares
with graphical user interfaces (GUIs).
