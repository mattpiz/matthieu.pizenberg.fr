+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Computer Vision",
    "Multimedia",
    "Robotics",
    "Tennis",
    "Raclette"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Master in Computer Science and Applied Mathematics"
  institution = "ENSEEIHT Toulouse"
  year = 2015

+++

# About me

Hi!, my name is Matthieu and I'm a French Ph.D. student located in Singapore.
I've always been passionate about science and technology,
quite early, I was spending hours on building Lego and Meccano constructions.
These days I'm more focused on digital content, but still love
getting my hands dirty for some side robotics projects.

In my PhD, I deal with computer vision and multimedia.
I've also had a growing interest for functional programming (elm) and machine learning.

I also strongly believe in sharing knowlegde and open source community,
which makes me sometimes a little sad when we think about accessibility of research work.
