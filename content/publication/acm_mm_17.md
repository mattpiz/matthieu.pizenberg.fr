+++
abstract = "Interactive segmentation consists in building a pixel-wise partition of an image, into foreground and background regions, with the help of user inputs. Most state-of-the-art algorithms use scribble-based interactions to build foreground and background models, and very few of these work focus on the usability of the scribbling interaction. In this paper we study the outlining interaction, which is very intuitive to non-expert users on touch devices. We present an algorithm, built upon the existing GrabCut algorithm, which infers both  foreground and background models out of a single outline. We conducted a user study on 20 participants to demonstrate the usability of this interaction, and its performance for the task of interactive segmentation."
abstract_short = ""
authors = ["Matthieu Pizenberg", "Axel Carlier", "Emmanuel Faure", "Vincent Charvillat"]
date = "2017-10-26"
image_preview = ""
math = true
publication_types = ["1"]
publication = "ACM Multimedia (ACM MM)"
publication_short = "ACM MM"
selected = true
title = "Outlining Objects for Interactive Segmentation on Touch Devices"
url_code = "https://github.com/mpizenberg/otis"
url_dataset = ""
url_pdf = "https://mpizenberg.github.io/resources/otis/outlining-objects-interactive.pdf"
url_project = ""
url_slides = ""
url_video = ""

[[url_custom]]
name = "Poster"
url = "https://mpizenberg.github.io/resources/otis/poster-mm17.pdf"

[[url_custom]]
name = "Online demo of user study"
url = "http://mm17-otis.pizenberg.fr"

# Optional featured image (relative to `static/img/` folder).
[header]
image = "publications/acmmm_17.jpg"
caption = ""

#More detail can easily be written below using *Markdown* and $\rm \LaTeX$ math code.
+++
