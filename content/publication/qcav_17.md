+++
abstract = "We present a photometric stereo-based system for retrieving the RGB albedo and the fine-scale details of an opaque surface. In order to limit specularities, the system uses a controllable diffuse illumination, which is calibrated using a dedicated procedure. In addition, we rather handle RAW, non-demosaiced RGB images, which both avoids uncontrolled operations on the sensor data and simplifies the estimation of the albedo in each color channel and of the normals. We finally show on real-world examples the potential of photometric stereo for the 3D-reconstruction of very thin structures from a wide variety of surfaces."
abstract_short = ""
authors = ["Yvain Quéau", "Matthieu Pizenberg", "Jean-Denis Durou", "Daniel Cremers"]
date = "2017-05-10"
image_preview = ""
math = true
publication_types = ["1"]
publication = "International Conference on Quality Control by Artificial Vision (QCAV)"
publication_short = "QCAV"
selected = true
title = "Microgeometry capture and RGB albedo estimation by photometric stereo without demosaicing"
url_code = "https://github.com/yqueau/microgeometry_ps"
url_dataset = ""
url_pdf = "https://www.irit.fr/~Jean-Denis.Durou/PUBLICATIONS/qcav_2017.pdf"
url_project = ""
url_slides = ""
url_video = ""

# [[url_custom]]
# name = "Custom Link"
# url = "http://www.example.org"

# Optional featured image (relative to `static/img/` folder).
[header]
image = "publications/qcav_17.jpg"
caption = ""

#More detail can easily be written below using *Markdown* and $\rm \LaTeX$ math code.
+++
